import requests
import json
from auth import calc_access_token
from tokens import admin_read_refresh_token

def list_monitor():
    url = 'https://www.site24x7.com/api/monitors'
    headers = {'Accept':'application/json; version=2.0', 'Authorization':'Zoho-oauthtoken '+calc_access_token(admin_read_refresh_token)}
    r = requests.get(url, headers=headers)
    return r.json()

# Sample code:
# monitors = admin.list_monitor()
# for monitor in monitors['data']:
#   print(monitor)


