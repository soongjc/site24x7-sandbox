import requests
import json
from auth_secret import client_id, client_secret

def get_refresh_token(access_token_from_self_client):
    url = 'https://accounts.zoho.com/oauth/v2/token'
    data = {'client_id':client_id, 'client_secret':client_secret, 'code':access_token_from_self_client, 'grant_type':'authorization_code'}
    r = requests.post(url, data=data)
    return r.json()['refresh_token']

def calc_access_token(refresh_token):
    url = 'https://accounts.zoho.com/oauth/v2/token'
    data = {'client_id':client_id, 'client_secret':client_secret, 'refresh_token':refresh_token, 'grant_type':'refresh_token'}
    r = requests.post(url, data=data)
    return r.json()['access_token']