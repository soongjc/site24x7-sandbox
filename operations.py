import requests
import json
from auth import calc_access_token
from tokens import operations_read_refresh_token, operations_create_refresh_token


def list_maintenance():
    url = 'https://www.site24x7.com/api/maintenance'
    headers = {'Accept':'application/json; version=2.0', 'Authorization':'Zoho-oauthtoken '+calc_access_token(operations_read_refresh_token)}
    r = requests.get(url, headers=headers)
    return r.json()

# Sample Code:
# result = operations.list_naintenance()
# print(result)

def create_maintenance(data):
    data = json.dumps(data)
    url = 'https://www.site24x7.com/api/maintenance'
    headers = {'Accept':'application/json; version=2.0', 'Authorization':'Zoho-oauthtoken '+calc_access_token(operations_create_refresh_token)}
    r = requests.post(url, data=data, headers=headers)
    return r.json()