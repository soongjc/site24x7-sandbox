This is a sandbox created to test the site24x7 api. 

**Authentication**

The authentication process involves several steps. 

From site24x7.com:
1. You need to create a **Self Client** and get the 'client_id' and 'client_secret' from the **Zoho Developer Console**. [(ref)](https://www.site24x7.com/help/api/#getting-started)
2. Generate the **grant access token** from the **Generate Code tab**. You need to enter the **oathscope** [(ref)](https://www.site24x7.com/help/api/#authentication). For example, to do a maintanence request you need to use the 'Site24x7.operations.read' or 'Site24x7.operations.create' scope to generate the token. 
3. Save your 'client_id', 'client_secret', and 'grant access token'. Remember, this grant access token is only valid for at most 10 mins, so we need to get the **refresh token** instead. Refresh token is permanent, and it can be used to calculate the access token for each API call. The calculation is done using the function 'calc_access_token()' in *auth.py*.

Get refresh token:
1. Paste your 'client_id' and 'client_secret' in the *auth_secret.py* file
2. Get the 'refresh token' by using the function 'get_refresh_token()' from the *auth.py* file. Pass your **access token (operations, admin, account etc)** as a parameter.

   Sample Code:
   ```
     response = get_refresh_token(1000.xxx)
     print(response)
   ```
3. Get your 'refresh token' from the terminal and paste it in *tokens.py*. The token naming convention follows the scope, e.g. Site24x7.operations.read => operations_read_refresh_token.
Note: You may change the naming convention but you need to modify the tasks files (*operations.py*, *admin.py* etc) to import the tokens correctly


**Usage**

You can use the *main.py* file to test the functions.
Sample Code:
```   
   import operations

   data = {'maintenance_type':'3', 
   'selection_type':'2', 
   'start_time':'19:00', 
   'end_time':'20:00', 
   'display_name':'Once maintenance', 
   'description':'Testing', 
   'monitors':['126864000013580009'], # get monitor_id from admin.list_monitor() or from the url of the monitor dashboard page
                                      # 126864000013580009 is the id for HL Connect Mobility (Athena)
   'start_date':'2019-10-03', 
   'end_date':'2019-10-03'}

   result = operations.create_maintenance(data)
   print(result['message'])
```
You will receive a json indicating if the call is successful or not. Another way to verify is to check the maintenance dashboard and see if the maintenance has been booked.


**Environment**

To test this code, you can either 
1. Run in your local environment with python **requests** package installed ('pip install requests').
2. Use .gitlab-ci.yml to run the Gitlab CI/CD pipeline.
3. Use Jenkinsfile to run the pipeline in Jenkins. 